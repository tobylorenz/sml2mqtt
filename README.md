# Introduction

SML2MQTT uses the libsml library to get OBIS data and push it to MQTT.

# Build on Linux (e.g. Debian Testing)

Building under Linux works as usual:

    mkdir build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX=/usr ..
    cmake --build . --target all
    ctest
    cpack

# Tests

Static tests are

* Cppcheck (if OPTION_RUN_CPPCHECK is set)
* CCCC (if OPTION_RUN_CCCC is set)

Dynamic tests are

* Unit tests (if OPTION_BUILD_TESTS is set)
* Example runs (if OPTION_BUILD_EXAMPLES is set)
* Coverage (if OPTION_ADD_LCOV is set)

# Standards

* SML
* OBIS
* MQTT topics roughly follow the Homie Convention https://github.com/homieiot/convention in terms
  of qos=1, retain=yes, $name, and $unit, $state.
