/*
 * Copyright (C) 2018 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

/* C includes */
#include <unistd.h>
#ifdef WITH_SYSTEMD
#include <systemd/sd-daemon.h>
#endif

/* C++ includes */
#include <atomic>
#include <chrono>
#include <csignal>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <mosquittopp.h>
#include <thread>
#include <vector>

/* project internal includes */
#include "MqttClient.h"

/** abort the main loop */
std::atomic<bool> abortLoop;

/** handle SIGTERM */
void signalHandler(int /*signum*/)
{
    abortLoop = true;
}

/** main function */
int main(int argc, char ** argv)
{
    /* default parameters */
    std::string host = "localhost";
    int port = 1883;
    int qos = 1;
    std::string topic = "heizung/zaehler";
    std::string id = "collectd-sml";
    std::string username = "";
    std::string password = "";
    std::string collectd_identifier = "heizung/zaehler"; // host "/" plugin ["-" plugin instance]

    /* evaluate command line parameters */
    int c;
    while ((c = getopt (argc, argv, "h:p:q:t:i:u:P:d:c:")) != -1) {
        switch (c) {
        case 'h':
            host = optarg;
            break;
        case 'p':
            port = std::stoul(optarg);
            break;
        case 'q':
            qos = std::stoul(optarg);
            break;
        case 't':
            topic = optarg;
            break;
        case 'i':
            id = optarg;
        case 'u':
            username = optarg;
            break;
        case 'P':
            password = optarg;
            break;
        case 'c':
            collectd_identifier = optarg;
            break;
        default:
            std::cerr << "Usage: sml2mqtt [-h host] [-p port] [-q qos] [-t topic] [-i id] [-u username] [-P password] [-c collectd-identifier]" << std::endl;
            return EXIT_FAILURE;
        }
    }

    /* register signal handler */
    abortLoop = false;
    signal(SIGTERM, signalHandler);

    /* mosquitto constructor */
    if (mosqpp::lib_init() != MOSQ_ERR_SUCCESS) {
        std::cerr << "main: lib_init failed" << std::endl;
        return EXIT_FAILURE;
    }

    /* start MqttClient */
    MqttClient * mqttClient = new MqttClient(host.c_str(), port, qos, topic.c_str(), id.c_str(), username.c_str(), password.c_str(), "#");

#ifdef WITH_SYSTEMD
    /* systemd notify */
    sd_notify(0, "READY=1");
#endif

    /* start subscribe loop */
    std::chrono::time_point<std::chrono::system_clock> tp = std::chrono::system_clock::now();
    while(!abortLoop) {
        /* sleep */
        tp += std::chrono::seconds(10);
        std::this_thread::sleep_until(tp);

#ifdef WITH_SYSTEMD
        /* systemd notify */
        sd_notify(0, "WATCHDOG=1");
#endif

        /* read topics and write to collectd */
        bool ready = mqttClient->getTopic("$state") == "ready";
        static const std::vector<std::string> counters = {
            "1-0:1.8.0",
            "1-0:1.8.1",
            "1-0:1.8.2"
        };
        for(std::string counter : counters) {
            if (ready) {
                double valueFloat = std::stod(mqttClient->getTopic(counter + "*255", "0"));
                /* collectd doesn't like counters of type float, so we need to have integer */
                /* luckily collectd prefers to have something in seconds, rather than hours */
                /* so convert Wh(ours) to Ws(econds) */
                std::cout << "PUTVAL " << collectd_identifier << "/counter-" << counter << " interval=10 N:"
                          << std::fixed << std::setprecision(0) << (valueFloat * 3600.0) << std::endl;
            } else {
                std::cout << "PUTVAL " << collectd_identifier << "/counter-" << counter << " interval=10 N:U" << std::endl;
            }
        }
    }

    /* delete resources */
    delete mqttClient;

    /* mosquitto destructor */
    if (mosqpp::lib_cleanup() != MOSQ_ERR_SUCCESS) {
        std::cerr << "main: lib_cleanup failed" << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
